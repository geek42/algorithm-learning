#define LIST_MAX_SIZE 512

typedef int node;
typedef struct {
    unsigned int length;
    node bucket[LIST_MAX_SIZE];
} list;

list * list_create(unsigned int);
int list_empty(list *);
int list_clear(list *);
int list_index(list *, unsigned int, node *);
int list_find(list *, node *);
int list_insert(list *, unsigned int, node *);
int list_delete(list *, unsigned int);
unsigned int list_length(list *);
